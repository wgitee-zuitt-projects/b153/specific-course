import { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import {Card, Button} from 'react-bootstrap'

export default function SpecificCourse(){
	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState("")
	//useParams contains any values that we are trying to pass in the URL, stored as a key/value pair
	//e.g.
	//courseId: 620ba535d7c30a4ac5e4f7d5

	const { courseId } = useParams();

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
		.then(res => res.json())
		.then(data => {
			//console.log(data)
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})
	}, [])
	
	//console.log(courseId)
	return(
		<Card className="my-3">
			<Card.Header className="bg-dark text-white text-center pb-0">
				<h4>{name}</h4>
			</Card.Header>
			<Card.Body>
				<Card.Text>{description}</Card.Text>
				<h6>Price: {price}</h6>
			</Card.Body>
			<Card.Footer className="d-grid gap-2">
				<Button variant="primary" block="true">Enroll</Button>
			</Card.Footer>
		</Card>

	)
}